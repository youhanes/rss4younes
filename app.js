
const express = require('express');
const app = express();

const cors = require('cors');
app.use(cors());

let Parser = require('rss-parser');
let parser = new Parser();



app.get('/api/news', async (req, res) => {
    let feed = await parser.parseURL('https://dev98.de/feed/');
    res.json(feed);
})


app.listen(3000, () => console.log('server is running on port 3000...'));